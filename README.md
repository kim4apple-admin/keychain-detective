# Keychain Detective

keychaindetective prints and decodes access control lists from the macOS keychain. It currently works on private keys or generic password items / identity preferences.

It is distributed in an macOS app bundle because that makes it easier. The GUI app doesn't do anything, and the command line tool is in the app bundle under Contents/MacOS/keychaindetective.

## Downloading
Get a binary in the downloads section.


## Usage:
    keychaindetective -l <label of private key or generic password> 

Example:

    /Users/tperfitt/Desktop/Keychain\ Detective.app/Contents/MacOS/keychaindetective  -l TCSCertRequestKey
    Keys
        Key 0:
            ACL 0:
                Description: TCS Certificate Request Private Key
                Authorization #0: ACLAuthorizationEncrypt
                All Trusted Applications Allowed (value is nil)
            ACL 1:
                Description: TCS Certificate Request Private Key
                Authorization #0: ACLAuthorizationDecrypt
                Authorization #1: ACLAuthorizationDerive
                Authorization #2: ACLAuthorizationExportClear
                Authorization #3: ACLAuthorizationExportWrapped
                Authorization #4: ACLAuthorizationMAC
                Authorization #5: ACLAuthorizationSign
                Trusted Applications: 3
                    App 0: /Users/tperfitt/Library/Developer/Xcode/DerivedData/TCSCertRequest-bzmkdbwyvtmykzdcetdioyrmiftu/Build/Products/Debug/tcscertrequest
                    App 1: /System/Library/SystemConfiguration/EAPOLController.bundle/Contents/Resources/eapolclient
                    App 2: group://AirPort
            ACL 2:
                Description: 4a0cf29e4f9102e835366bf0d6736f439150abbed9d835293dfcbe691081f2ea
                Authorization #0: ACLAuthorizationIntegrity
                All Trusted Applications Allowed (value is nil)
            ACL 3:
                Description: 3c3f786d6c2076657273696f6e3d22312e302220656e636f64696e673d225554462d38223f3e0a3c21444f435459504520706c697374205055424c494320222d2f2f4170706c652f2f44544420504c49535420312e302f2f454e222022687474703a2f2f7777772e6170706c652e636f6d2f445444732f50726f70657274794c6973742d312e302e647464223e0a3c706c6973742076657273696f6e3d22312e30223e0a3c646963743e0a093c6b65793e506172746974696f6e733c2f6b65793e0a093c61727261793e0a09093c737472696e673e6170706c653a3c2f737472696e673e0a093c2f61727261793e0a3c2f646963743e0a3c2f706c6973743e0a
                Decoded Description:
                ================================================
    <?xml version="1.0" encoding="UTF-8"?>
    <!DOCTYPE plist PUBLIC "-//Apple//DTD PLIST 1.0//EN" "http://www.apple.com/DTDs/PropertyList-1.0.dtd">
    <plist version="1.0">
    <dict>
	    <key>Partitions</key>
	    <array>
		    <string>apple:</string>
	    </array>
    </dict>
    </plist>
    
                ================================================
                Authorization #0: ACLAuthorizationPartitionID
                All Trusted Applications Allowed (value is nil)
            ACL 4:
                Description: TCS Certificate Request Private Key
                Authorization #0: ACLAuthorizationChangeACL
                Trusted Applications: 2
                    App 0: /Users/tperfitt/Library/Developer/Xcode/DerivedData/TCSCertRequest-bzmkdbwyvtmykzdcetdioyrmiftu/Build/Products/Debug/tcscertrequest
                    App 1: /System/Library/SystemConfiguration/EAPOLController.bundle/Contents/Resources/eapolclient
    
 